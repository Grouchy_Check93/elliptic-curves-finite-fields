from elliptic import *
from fractions import Fraction as frac

from finitefield.finitefield import FiniteField

import itertools


def findPoints(curve, field):
    print('Finding all points over %s' % (curve))
    print('The ideal generator is %s' % (field.idealGenerator))

    degree = field.idealGenerator.degree()
    subfield = field.primeSubfield
    xs = [field(x) for x in itertools.product(range(subfield.p), repeat=degree)]
    ys = [field(x) for x in itertools.product(range(subfield.p), repeat=degree)]

    points = [Point(curve, x, y) for x in xs for y in ys if curve.testPoint(x, y)]
    return points


FFd = FiniteField(67, 1)    # F_67
curve = EllipticCurve(a=FFd(2), b=FFd(3))   # Curve params

G = Point(curve, FFd(2), FFd(22))   # G : pub key
n_a = 4  # Private key of A (n_a < n)
n_b = 4  # Private key of B (n_b < n)
k = 2  # k is a random int the sender selects when encrypting (k < n)

P_m = Point(curve, FFd(24), FFd(26))    # Point encoded plaintext msg

print("\n\n\n=========\n\n\nEl Gamal\n\n\n=========\n\n\n")
# Pvt key of A
print("***************\nCalculating for A:\n***************\n")
P_a = n_a*G
print("\nPub key of A : ", P_a)
# Pvt key of B
print("\n***************\nCalculating for B:\n***************\n")
P_b = n_b * G
print("\nPub Key of B :", P_b)


print("\nB is sending to A (Encrypting with A's pub key)\n")
print("\n***************\nCiphertexts:\n***************\n\n C1=k*G;  C2=P_m + k*P_a\n")
C1 = k*G
C2 = P_m + k*P_a
print("\nC1 : {} \n".format(C1))
print("\nC2 : {} \n".format(C2))
print("\nC = [ {}, {} ]\n".format(C1, C2))

print("\nA is decrypting\n")
P_m_dec = C2 - C1*n_a
print("\nDecrypted plaintext C2 - C1*n_a : {} \n".format(P_m_dec))
